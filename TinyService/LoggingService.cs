﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Net;

namespace TinyService
{
    class LogEntry
    {
        private string data;

        public LogEntry(DateTime dateTime, HttpListenerRequest request)
        {
            this.data = dateTime.ToString() + " " +
                        request.RemoteEndPoint + " " +
                        request.HttpMethod + " " +
                        request.RawUrl;

        }

        public string Data
        {
            get { return this.data; }
        }
    }

    class LoggingService
    {
        Queue<LogEntry> logs;

        /*
         * Constr
         */
        public LoggingService()
        {
            logs = new Queue<LogEntry>();
        }


        /*
         * Request for a log entry to be written to file
         */
        public void Enqueue(LogEntry item)
        {
            lock (this)
            {
                logs.Enqueue(item);
                Monitor.Pulse(this);
            }
        }


        /*
         * Gets the next log entry
         */
        public LogEntry Dequeue()
        {
            lock (this)
            {
                if (0 == logs.Count)
                {
                    Monitor.Wait(this);
                }
                return logs.Dequeue();
            }
        }


        /*
         * Writes log entries to file
         */
        public void LogToFile()
        {
            while (true)
            {
                string log = Dequeue().Data;
                FileStream file = File.Open("service.log", FileMode.Append);
                byte[] buffer = Encoding.UTF8.GetBytes(log + "\n");
                file.Write(buffer, 0, buffer.Length);
                file.Close();
            }
        }


        /*
         * Starts logging service
         */
        public void Start()
        {
            Thread th = new Thread(this.LogToFile);
            th.Start();
        }
    }
}
