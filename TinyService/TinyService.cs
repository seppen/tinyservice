﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Resources;

namespace TinyService
{
    class TinyService
    {
        private HttpListener listener;
        private Thread listeningThread;
        private string fileName;
        private LoggingService loggingService;


        /*
         * Constr
         */
        public TinyService(string fileName, string prefixes)
        {
            this.listener = new HttpListener();
            this.loggingService = new LoggingService();
            this.fileName = fileName;
            this.listener.Prefixes.Add(prefixes);
            this.listeningThread = new Thread(ListenForClients);
        }


        /*
         * Starts TinyService
         */
        public void Start()
        {
            Console.WriteLine("Starts logging service...");
            this.loggingService.Start();
            Console.WriteLine("Starts listening...");
            this.listeningThread.Start();
        }


        /*
         * Listens for clients
         */
        private void ListenForClients()
        {
            this.listener.Start();

            while (true)
            {
                //waits for a client
                HttpListenerContext context = this.listener.GetContext();
                LogEntry log = new LogEntry(DateTime.Now, context.Request);
                Console.WriteLine(log.Data);
                this.loggingService.Enqueue(log);
                ThreadPool.QueueUserWorkItem(HandleClientRequest, context);
            }
        }


        /*
         * Handles client request
         */
        private void HandleClientRequest(object context)
        {
            HttpListenerContext httpContext = (HttpListenerContext)context;
            HttpListenerRequest request = httpContext.Request;
            HttpListenerResponse response = httpContext.Response;

            string responseString = "";

            switch (request.RawUrl)
            {
                case "/":
                    responseString = this.RootUrlResponse();
                    break;
                case "/new":
                    responseString = this.NewUrlResponse(request);
                    break;
                case "/style.css":
                    responseString = this.StyleResponse();
                    break;
                default:
                    responseString = this.NotFoundResponse();
                    break;
            }

            this.Respond(response, responseString);
        }
        

        /*
         * Performs a response to request
         */
        private void Respond(HttpListenerResponse response, string data)
        {
            Stream output = response.OutputStream;
            byte[] buffer = Encoding.UTF8.GetBytes(data);
            output.Write(buffer, 0, buffer.Length);
            output.Close();
        }


        /*
         * GET or POST to /new
         */
        private string NewUrlResponse(HttpListenerRequest request)
        {
            string flash = "";
            if ("POST" == request.HttpMethod)
            {
                Dictionary<string, string> post = this.GetRequestPostData(request);
                if (post.Keys.Contains("task"))
                {
                    this.WriteDataToFile(DateTime.Now.ToLongTimeString() + " " + post["task"]);
                    flash = "record added";
                }
            }
            return Resource.form.Replace("{flash}", flash);
        }


        /*
         * GET or POST to /
         */
        private string RootUrlResponse()
        {
            string tasks = "";
            lock (this)
            {
                StreamReader reader = new StreamReader(File.Open(fileName, FileMode.OpenOrCreate),
                                                       Encoding.UTF8);

                while (!reader.EndOfStream)
                {
                    tasks += "<p>" + reader.ReadLine() + "</p>";
                }
                reader.Close();
            }
            return Resource.list.Replace("{tasks}", tasks);
        }


        /*
         * GET or POST to /style.css
         */
        private string StyleResponse()
        {
            return Resource.style;
        }


        /*
         * GET or POST to something not available
         */
        private string NotFoundResponse()
        {
            return "not found";
        }


        /*
         * Extracts post data from request
        */
        private Dictionary<string, string> GetRequestPostData(HttpListenerRequest request)
        {
            StreamReader reader = new StreamReader(request.InputStream, Encoding.UTF8);
            string[] keyValues = reader.ReadToEnd().Split(new char[] { '=', '&' });
            reader.Close();
            Dictionary<string, string> post = new Dictionary<string, string>();
            for (int i = 0; i < keyValues.Length; i += 2)
            {
                post.Add(keyValues[i], System.Web.HttpUtility.UrlDecode(keyValues[i + 1], Encoding.UTF8));
            }
            return post;
        }


        /*
         * Writes to file
         */
        private void WriteDataToFile(string data)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(data + "\n");
            lock (this)
            {
                FileStream file = File.Open(fileName, FileMode.Append);
                file.Write(buffer, 0, buffer.Length);
                file.Close();
            }
        }
    }
}
